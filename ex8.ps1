$LIMIT = 13

# load the given number into memory
$NUMBER = Get-Content ex8_data.txt

function main() {
	$res = ""				# the digits that form the product
	$product = 0

	# go through each of the number's digits
	for ($i = 0; $i -lt ($NUMBER.Length - $LIMIT); $i++) {
		$tmpProd = 1
		$tmp = ""
		# form a number of 13 digits and get the product of those digits
		for ($j = $i; $j -lt ($i + $LIMIT); $j++) {
			if ([System.Int32]::Parse($NUMBER[$j]) -eq 0) {
				break 
			}

			$tmp += $NUMBER[$j]
			$tmpProd *= [System.Int32]::Parse($NUMBER[$j])
		}

		# if the product is greater than the current greatest product, replace it
		if ($tmpProd -gt $product) {
			$res = $tmp
			$product = $tmpProd
		}
	}

	Write-Output ("Digits: " + $res)
	Write-Output ("Product: " + $product)
}

main