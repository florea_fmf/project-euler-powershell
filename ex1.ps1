function main {
	$LIMIT = 1000

	# array that contains all multiples of 3 and 5
	$multiples = @()
	$sum = 0

	# put all multiples of 3 in our array
	for ($i = 0; $i -lt $LIMIT; $i += 3) {
		$multiples += $i
	}

	# put all multiples of 5 in our array
	for ($i = 0; $i -lt $LIMIT; $i += 5) {
		$multiples += $i
	}

	# sort all multiples of 3 or 5 and keep only the unique ones
	$multiples = ($multiples | Sort-Object | Get-Unique)

	# get the sum of all multiples of 3 or 5
	foreach ($number in $multiples) {
		$sum += $number
	}

	# write the result
	Write-Output $sum
}

main