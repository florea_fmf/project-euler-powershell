$LIMIT = 100

function main() {
	$sumOfSquares = 0
	$squareOfSum = 0

	for ($i = 1; $i -le $LIMIT; $i++) {
		$sumOfSquares += [System.Math]::Pow($i, 2)
		$squareOfSum += $i
	}
	$squareOfSum = [System.Math]::Pow($squareOfSum, 2)

	Write-Output ($squareOfSum - $sumOfSquares)
}

main