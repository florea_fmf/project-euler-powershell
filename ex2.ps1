$LIMIT = 4000000

function main {
	# init fibonacci numbers
	$a = 1
	$b = 2
	$tmp = 0
	# sum of even valued numbers
	$sum = 0

	while ($a -le $LIMIT) {
		# if number is even added to the sum
		if ($a % 2 -eq 0) {
			$sum += $a
		}

		# generate next fibonacci number in sequence
		$tmp = $a + $b
		$a = $b
		$b = $tmp
	}

	# write the result
	Write-Output $sum
}

main