# returns true if n is palindrome, false otherwise
# the idea is to compare the number as strings
function isPalindrome($n) {
	# transform the number in a char array
	$temp = $n.toString().toCharArray()
	# reverse the array
	[array]::Reverse($temp)
	# make the reversed array a string again
	$temp = -join($temp)

	# compare the reversed string to the original one
	if ($n.toString() -eq $temp) {
		return $true
	} else {
		return $false
	}
}

function main() {
	# the number we are looking for
	$largestPalindrome = 0

	# since we need the product of two 3-digit numbers we need to go from [100-999]
	for ($i = 999; $i -gt 99; $i--) {
		for ($j = 999; $j -gt 99; $j--) {
			# get the current candidate
			$number = $i * $j

			# if the current candidate is lower than the current largest palindrome
			# there is no need to check if it is a palindrome, alse all numbers
			# following this one will be lower so we don't need to check the
			# rest of the numbers in the current j loop
			if ($number -lt $largestPalindrome) {
				break
			}

			# if the current candidate is bigger than the current largest palindrome
			# check if it is a palindrome
			if (isPalindrome($number) -eq $true) {
				$largestPalindrome = $number
			}
		}
	}

	# show the result
	Write-Output $largestPalindrome
}

main