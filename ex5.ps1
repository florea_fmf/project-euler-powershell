$LIMIT = 20
$PRIMES = @(2, 3, 5, 7, 11, 13, 17, 19)

# the ideea is to factorise each number from 1 to Limit and then
# multiply the unique bases to the hihest power

# we need to have a custom class that represents the number as a^b
# for easier comparisons
class NumberWithPower {
	[int]$Base
	[int]$Power

	# constructor
	NumberWithPower(
		[int]$base,[int]$power
	) {
		$this.Base = $base
		$this.Power = $power
	}

	[string]ToString() {
		return ("{0}^{1}" -f $this.Base, $this.Power)
	}
}

# returns the prime factorisation of the input
function getPrimeFactorisation($n) {
	$descPrimes = @()
	while ($n -gt 1) {
		foreach ($prime in $PRIMES) {
			if ($n % $prime -eq 0) {
				$descPrimes += $prime
				$n /= $prime
			}
		}
	}
	$descPrimes = $descPrimes | Sort-Object

	# converts the factorisation as a number with power
	# e.g: 10 is now 2 5, will be 2^1, 5^1
	$i = 1
	$base = $descPrimes[0]
	$power = 0
	$res = @([NumberWithPower]::new($base, $power))
	for ($i = 0; $i -lt $descPrimes.Count; $i++) {
		if ($base -ne $descPrimes[$i]) {
			($res[$res.count - 1]).Base = $base
			($res[$res.count - 1]).Power = $power

			$base = $descPrimes[$i]
			$power = 1
			$res += [NumberWithPower]::new($base, $power)
		} else {
			$power++
		}
	}
	($res[$res.count - 1]).Base = $base
	($res[$res.count - 1]).Power = $power

	return $res
}

function main() {
	$tempRes = @()

	for ($i = 2; $i -le $LIMIT; $i++) {
		$factors = getPrimeFactorisation $i
		# Write-Output ($i.ToString() + " - " + $factors)

		# only keep the number with a unique base to the highest power
		foreach ($factor in $factors) {
			for ($j = 0; $j -lt $tempRes.Count; $j++) {
				if ($tempRes[$j].Base -eq $factor.Base) {
					if ($tempRes[$j].Power -lt $factor.Power) {
						$tempRes[$j].Power = $factor.Power
					}
				}
			}

			if ($tempRes[$tempRes.Count - 1].Base -lt $factor.Base) {
				$tempRes += $factor
			}
		}
	}

	# get and show the final result
	$res = 1
	foreach ($num in $tempRes) {
		$res *= [System.Math]::Pow($num.Base, $num.Power)
	}

	Write-Output $res
}

main