$LIMIT = 10001

# returns all primes up to the given parameter by using the sieve of eratosthenes
function getPrimes($n) {
	$primes = 0..$n

	for ($i = 2; $i * $i -lt $n; $i++) {
		# if the current element is unchanged then it is a prime
		if ($primes[$i] -ne -1) {
			# mark all numbers that are his multiples as non-prime
			for ($j = $i * $i; $j -lt $n; $j += $i) {
				$primes[$j] = -1
			}
		}
	}

	# generate the array containing only the prime numbers
	$res = @()
	for ($i = 2; $i -lt $n; $i++) {
		if ($primes[$i] -ne -1) {
			$res += $primes[$i]
		}
	}

	return $res
}

# returns the nth prime number
function getNthPrime($n) {
	# from the prime number theory and Rosser's theorem we have:
	#   p(n) < n(log(n) + log(log(n))) for n >= 6
	# we know that the nth prime number is smaller than this upper bound
	#   further we will generate all prime numbers up to this upper bound by
	#   using a sieve, then extract the nth prime number
	$upperBound = [math]::Round($n * ([math]::Log($n) + [math]::Log([math]::Log($n))))

	$primes = getPrimes $upperBound

	return $primes[$n-1]
}

function main() {
	Write-Output (getNthPrime $LIMIT)
}

main