﻿$LIMIT = 1000

# check if a given triplet is pythagorean triplet
# it assumes that a < b < c
function isPythTriplet([int]$a, [int]$b, [int]$c) {
    if (([Math]::Pow($a, 2) + [Math]::Pow($b, 2)) -eq [Math]::Pow($c, 2)) {
        return $true
    } else {
        return $false
    }
}

function main() {
    # generate all triplest that
    #   1. a < b < c
    #   2. a + b + c = LIMIT
    $a = 1
    $b = $a + 1
    while ($a -le $LIMIT / 2) {   # if a > LIMIT / 2 then it cannot satisfy both conditions
        while ($b -le $LIMIT - $a - $b) {   # if b > (LIMIT-a-b) then c < b wich doesn't satisfy 1.
            $c = $LIMIT - $a - $b

            if (isPythTriplet $a $b $c) {
                Write-Output ("Result: " + ($a * $b * $c).ToString())
                break
            }

            $b++
        }

        $a++
        $b = $a + 1
    }
}

main
