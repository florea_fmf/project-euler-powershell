$LIMIT = 600851475143

# checks if the given number is prime
function isPrime($n) {
    # if it's divisible by 2 then it's not prime
    if ($n % 2 -eq 0) {
        return $false
    }

    # we check if it is divisible by any odd number
    for ($i = 3; $i -le [math]::Sqrt($n); $i += 2) {
        if ($n % $i -eq 0) {
            return $false
        }
    }

    return $true
}

function main() {
    # starting from the highest possible divisor
    for ($i = [math]::Floor([math]::Sqrt($LIMIT)); $i -gt 1; $i--) {
        # we check if this number is a divisor
        if ($LIMIT % $i -eq 0) {
            # we check if it is a prime number
            if (isPrime($i)) {
                # by following this logic the first number that fullfils
                # all this conditions is the one we are looking for
                Write-Output $i
                return
            }
        }
    }
}

main